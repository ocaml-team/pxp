pxp (1.2.9-4) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * Fix compilation with OCaml 5.2.0 (Closes: #1073909)

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)

 -- Stéphane Glondu <glondu@debian.org>  Sat, 10 Aug 2024 05:36:05 +0200

pxp (1.2.9-3) unstable; urgency=medium

  * Team upload
  * Remove explicit dependency on ocaml-nox-$ABI
  * Switch to dh, bump debhelper compat level to 13
  * Bump Standards-Version to 4.6.0
  * Bump debian/watch version to 4
  * Add Rules-Requires-Root: no

 -- Stéphane Glondu <glondu@debian.org>  Tue, 25 Jan 2022 16:16:27 +0100

pxp (1.2.9-2) unstable; urgency=medium

  [ Stéphane Glondu ]
  * Team upload
  * Update Vcs-*

  [ Christopher Cramer ]
  * Fix compilation with OCaml 4.08.1 (Closes: #944593)

 -- Stéphane Glondu <glondu@debian.org>  Thu, 21 Nov 2019 13:29:18 +0100

pxp (1.2.9-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Bump Standards-Version to 4.0.0

 -- Stéphane Glondu <glondu@debian.org>  Sat, 29 Jul 2017 08:17:23 +0200

pxp (1.2.8-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update Vcs-*
  * Bump Standards-Version to 3.9.8

 -- Stéphane Glondu <glondu@debian.org>  Fri, 12 Aug 2016 14:19:11 +0200

pxp (1.2.7-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Switch debian/copyright to format 1.0
  * Bump Standards-Version to 3.9.6
  * Bump debhelper compat level to 9

 -- Stéphane Glondu <glondu@debian.org>  Mon, 04 May 2015 20:36:54 -0300

pxp (1.2.4-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Update Vcs-*

 -- Stéphane Glondu <glondu@debian.org>  Thu, 11 Jul 2013 11:21:26 +0200

pxp (1.2.2-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Remove Stefano from Uploaders
  * Bump Standards-Version to 3.9.2
  * Switch source package format to 3.0 (quilt)
  * Update debian/watch

 -- Stéphane Glondu <glondu@debian.org>  Sat, 23 Jul 2011 18:25:53 +0200

pxp (1.2.1-2) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * debian/control
    - bump Standards-Version to 3.8.3 (no chances needed)
    - update Homepage field to a more specific location
    - add missing ${misc:Depends} on libpxp-ocaml-dev
  * debian/rules:
    - avoid using ocamldoc-apiref-config (which is now gone), instead ship
      a hand-written doc-base entry for upstream doc (Closes: #549750)
    - switch to dh_ocaml for dependency generation, bump deps on dh-ocaml
      accordingly. For the interim, keep old-style dependencies as well.
    - switch {debhelper,dpatch}.mk: fix debian/*.log cleanup

  [ Sylvain Le Gall ]
  * Clean examples/validate/Makefile.config

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 06 Oct 2009 16:09:50 +0200

pxp (1.2.1-1) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * New Upstream Version
  * Rebuild against OCaml 3.11, bump build-deps as needed
  * update watch file
  * debian/patches
    - remove patch build_w_camlp5 (no longer needed, pxp now builds fine
      with legacy camlp4 and ulex). As a consequence replace build-deps
      from camlp5/ulex08 to camlp4/ulex
    - add patch ocamldoc_missing_txt to avoid referencing an unavailable doc
  * debian/*.in use high level substvar instead of making assumptions on
    the stdlib dir location
  * bump debhelper compatibility to 7
  * debian/control
    - drop obsolete version specifications in build-deps
    - add build dep on dh-ocaml
    - bump Standards-Version to 3.8.1 (no changes)
    - move pxp to (new) archive section "ocaml"
  * debian/docs: avoid installing vanished documents
  * use upstream reference manual
  * debian/rules:
    - use ocaml.mk as a CDBS "rules" snippet
    - create destdir at the end of the build
    - generate doc-base entry for upstream ocamldoc API reference
  * debian/links: add link to preserve upstream manual dir structure

  [ Stephane Glondu ]
  * Switching packaging to git

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 16 Mar 2009 16:57:08 +0100

pxp (1.2.0~test1-3) unstable; urgency=low

  * debian/control: fix typo in package short description (closes: #446909)
  * setting me as an uploader, d-o-m as the maintainer
  * update standards-versions, no changes needed
  * add Homepage field
  * move expansion of upstream comments to ocamldoc-like comments to a
    separate shell script, to avoid Makefile escaping issues
  * debian/rules: use ocamlfind to pass -I flags down to ocamldoc and add
    includes related to the netstring package

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 30 Dec 2007 09:33:33 +0100

pxp (1.2.0~test1-2) unstable; urgency=low

  * debian/rules
    - enable generation of ocamldoc API reference (via CDBS)

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 09 Sep 2007 12:18:47 +0200

pxp (1.2.0~test1-1) experimental; urgency=low

  * new upstream release
  * rebuild with OCaml 3.10 (whereas using camlp5 and ulex 0.8)
  * debian/watch
    - add watch file (for the development version)
  * bump debhelper deps and compatibility level to 5
  * debian/control
    - add build-dep on camlp5, ocaml-ulex08
  * debian/patches/
    - renamed patches to avoid numbers in their name, 00list is enough
    - add patch build_w_camlp5, ensure pxp is buildable against camlp5

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 09 Aug 2007 16:49:18 -0400

pxp (1.1.96-8) unstable; urgency=low

  * debian/rules
    - use ocaml.mk
  * debian/control
    - bumped build dependency on ocaml-nox to >= 3.09.2-7, since we now use
      ocaml.mk

 -- Stefano Zacchiroli <zack@debian.org>  Sat,  4 Nov 2006 09:26:41 +0100

pxp (1.1.96-7) unstable; urgency=low

  * debian/control
    - bumped dependencies on ocamlnet

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 13 Sep 2006 17:40:03 +0200

pxp (1.1.96-6) unstable; urgency=low

  * debian/rules
    - avoid to create debian/control from debian/control.in on ocamlinit
    - removed from the source package files which are generated at build time
      from the corresponding .in files
  * debian/control.in
    - file removed, no longer needed

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  5 Sep 2006 22:36:25 +0200

pxp (1.1.96-5) unstable; urgency=low

  * Upload to unstable.

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 17 May 2006 02:33:12 +0000

pxp (1.1.96-4) experimental; urgency=low

  * Rebuilt against OCaml 3.09.2, bumped deps accordingly.
  * Bumped Standards-Version to 3.7.2 (no changes needed).

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 11 May 2006 23:38:22 +0000

pxp (1.1.96-3) unstable; urgency=low

  * Rebuilt against OCaml 3.09.1, bumped deps accordingly.

 -- Stefano Zacchiroli <zack@debian.org>  Sun,  8 Jan 2006 13:16:07 +0100

pxp (1.1.96-2) unstable; urgency=low

  * debian/control
    - tighter build-dep on ocamlnet, fixes FTBFS (closes: #340948)

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 27 Nov 2005 11:37:14 +0100

pxp (1.1.96-1) unstable; urgency=low

  * new upstream release
  * rebuilt with ocaml 3.09
  * debian/*
    - no more hardcoding of ocaml abi version anywhere
  * debian/rules
    - use cdbs

 -- Stefano Zacchiroli <zack@debian.org>  Sat, 26 Nov 2005 19:08:25 +0100

pxp (1.1.95-7) unstable; urgency=low

  * debian/control
    - rebuilt against latest netstring (bumped ocamlnet deps accordingly)
    - bumped ulex deps
    - bumped standards-version

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 31 Jul 2005 20:51:24 +0200

pxp (1.1.95-6) unstable; urgency=low

  * Rebuilt against ocaml 3.08.3
  * No longer built with wlex support (since wlex is no longer supported
    upstream and corresponding package has been removed from the debian
    archive)

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 29 Mar 2005 11:06:39 +0200

pxp (1.1.95-5) unstable; urgency=low

  * rebuilt against ocaml 3.08.2 (and ocamlnet 0.98-3, in turn rebuilt
    against ocaml 3.08.2)

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  9 Dec 2004 15:58:32 +0100

pxp (1.1.95-4) unstable; urgency=low

  * debian/control
    - forced build-dep against ocaml-ulex to version >= 0.5-2 (first
      version rebuilt against ocaml 3.08.2)

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  2 Dec 2004 15:48:32 +0100

pxp (1.1.95-3) unstable; urgency=low

  * rebuilt against ocaml 3.08.2

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 30 Nov 2004 19:00:24 +0100

pxp (1.1.95-2) unstable; urgency=medium

  * debian/control
    - bumped ocamlnet dependencies to >= 0.98-2 so that pxp get build
      unbugged version of ocamlnet 0.98
  * urgency set to medium, rationale: nothing serious has changed in pxp
    since the last upload and it has been in the archive for quite a
    long time

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 28 Sep 2004 10:21:47 +0200

pxp (1.1.95-1) unstable; urgency=low

  * New upstream release
    - added ulex based lexical analyzer
    - namespace support revised: it's now 100% compatible with W3C
      requirements and is now possible to remember the mapping of
      original prefixes to URIs
    - improved event-based parsing
    - added a camlp4 syntax extension to ease creation of XML trees
  * rebuilt against ocamlnet 0.98
  * dpatch-ified debian build process
  * debian/control
    - added dpatch Build-Dep
    - bumped ocamlnet Deps and Build-Deps to >= 0.98
    - added Deps and Build-Dep on ocaml-ulex
  * debian/rules
    - added dpatch targets and dependencies
    - cosmetic changes
  * debian/docs
    - installs preprocessor documentation
    - no longer installs (out of date) PXP manual

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  9 Sep 2004 15:37:31 +0200

pxp (1.1.94.2-4) unstable; urgency=low

  * rebuilt with ocaml 3.08
  * debian/control
    - bumped ocaml deps to 3.08
    - bumped standards-version to 3.6.1.1
    - changed ocaml deps to ocaml-nox
    - bumped ocamlnet deps to >= 0.97.1 (1st version rebuilt with 3.08)
    - bumped wlex deps to >= 20040706 (1st version rebuilt with 3.08)

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 26 Jul 2004 17:46:14 +0200

pxp (1.1.94.2-3) unstable; urgency=low

  * debian/control
    - fixed typo in ocamlnet dependencies: s/0.96-4/0.97-4/

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 18 Mar 2004 19:12:10 +0100

pxp (1.1.94.2-2) unstable; urgency=low

  * Rebuilt with pcre 5.06
  * debian/control
    - bumped (Build-)Dependency of ocamlnet to >= 0.96-4, which has been
      in turn rebuilt with pcre 5.06
    - bumped Standards-Version to 3.6.1.0

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 15 Mar 2004 19:31:37 +0100

pxp (1.1.94.2-1) unstable; urgency=low

  * New upstream release
    - include "validate" example fix (Closes: Bug#214049)

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 11 Nov 2003 18:24:00 +0100

pxp (1.1.94-5) unstable; urgency=low

  * Bumped ocaml-wlex dependencies

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  7 Oct 2003 16:53:20 +0200

pxp (1.1.94-4) unstable; urgency=low

  * Rebuilt with ocaml 3.07

 -- Stefano Zacchiroli <zack@debian.org>  Wed,  1 Oct 2003 13:58:20 +0200

pxp (1.1.94-3) unstable; urgency=low

  * Rebuilt with ocaml 3.07beta2

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 22 Sep 2003 17:16:29 +0200

pxp (1.1.94-2) unstable; urgency=low

  * debian/control
    - bumped ocamlnet dependencies to 0.96 so that inconsistencies
      between pxp and ocamlnet on architectures other than powerpc will
      be fixed
    - bumped Standards-Version to 3.6.0

 -- Stefano Zacchiroli <zack@debian.org>  Sun,  3 Aug 2003 11:54:02 +0200

pxp (1.1.94-1) unstable; urgency=low

  * New upstream release, notably:
    - rewritten Pxp_reader module (fixing bugs with relative URLs)
    - Pxp_yacc module split in four modules (old module kept for
      backward compatibility)
    - is now possibile to turn warnings in errors
    - event based parsing can now preprocess namespaces

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 23 Jun 2003 18:13:39 +0200

pxp (1.1.93-4) unstable; urgency=low

  * debian/control
    - bumped standards-version to 3.5.10
    - bumped dependencies on ocamlnet to 0.95
    - changed section to "libdevel"
  * debian/rules
    - removed DH_COMPAT in favour of debian/compat

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 17 Jun 2003 16:01:53 +0200

pxp (1.1.93-3) unstable; urgency=low

  * Removed Provides:.*-<version>
  * Changed dep on ocamlnet and wlex from virtual to real ones

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 10 Mar 2003 13:06:06 +0100

pxp (1.1.93-2) unstable; urgency=low

  * Libdir transition to /usr/lib/ocaml/3.06
  * Changed depends and build depends to ocaml{,-base}-3.06-1
  * Reformatted debian/copyright upstream author just to make lintian
    happy

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 16 Dec 2002 13:24:40 +0100

pxp (1.1.93-1) unstable; urgency=low

  * New upstream release
  * Bumped Standards-Version to 3.5.8
  * Added 'Provides: libpxp-ocaml-dev-<version>'
  * Changed Deps and Build-Deps from generic
    lib{ocamlnet,wlexing}-ocaml-dev to versioned
    lib{ocamlnet,wlexing}-ocaml-dev-<version>
  * Removed '-g' build flag

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 28 Nov 2002 23:58:17 +0100

pxp (1.1.92-7) unstable; urgency=low

  * Rebuilt against ocamlnet 0.94

 -- Stefano Zacchiroli <zack@debian.org>  Mon,  4 Nov 2002 11:25:01 +0100

pxp (1.1.92-6) unstable; urgency=low

  * bugfix: install wlex-utf8 *.o stuff
  * bugfix: added dep on libwlexing-ocaml-dev

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 17 Oct 2002 18:16:32 +0200

pxp (1.1.92-5) unstable; urgency=low

  * Bugfix: added build dep on ocaml-wlex

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 16 Oct 2002 13:55:54 +0200

pxp (1.1.92-4) unstable; urgency=low

  * Removed copied pxp_lib.ml file from diff.gz
  * Bugfix: added build depends on libwlexing-ocaml-dev

 -- Stefano Zacchiroli <zack@debian.org>  Fri,  4 Oct 2002 17:57:34 +0200

pxp (1.1.92-3) unstable; urgency=low

  * Patched to install /usr/lib/ocaml/pxp-lex-<enc>/pxp_lex_link_<enc>.o
    files, otherwise native code compilation does not work
  * Patched "readme" example makefile so that it doesn't use '-custom'
    option during native compilation (unexistent option)

 -- Stefano Zacchiroli <zack@debian.org>  Fri,  4 Oct 2002 16:29:26 +0200

pxp (1.1.92-2) unstable; urgency=low

  * Install all shipped examples (new event and pull parsers examples)
  * Patched "validate" example makefile for using 'pxp-lex-utf8' lexer
    because 'pxp-wlex-utf8' lexer is no longer available

 -- Stefano Zacchiroli <zack@debian.org>  Fri,  4 Oct 2002 12:15:40 +0200

pxp (1.1.92-1) unstable; urgency=low

  * New upstream release
  * Better test for ocamlopt existence in debian/rules

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  3 Oct 2002 21:45:47 +0200

pxp (1.1.91-1) unstable; urgency=low

  * New upstream release (development version)
  * Rebuilt against ocaml 3.06 (Closes: Bug#158253)
  * Switched to debhelper 4
  * Changed deps and build-deps to ocaml-3.06
  * Removed possibility to build against libnetstring which will not be
    rebuilt starting from ocaml 3.06
  * Changed from recommends to depends on ocaml-findlib
  * Removed mention of ocaml-pxp from debian/control; the new naming schema
    rulez!
  * Removed dangling symlinks from an example dir (Closes: Bug#152297)

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 26 Aug 2002 15:25:50 +0200

pxp (1.1.4-2) unstable; urgency=low

  * Change Depends: and Build-Depends:, now depends on netstring
    _or_ ocamlnet and build-depends only on ocamlnet
  * Fixed some Makefiles so that they remove some stuff generated
    by configure

 -- Stefano Zacchiroli <zack@debian.org>  Thu,  9 May 2002 16:39:18 +0200

pxp (1.1.4-1) unstable; urgency=low

  * New upstream release.

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 27 Mar 2002 20:47:53 +0100

pxp (1.1.3-4) unstable; urgency=low

  * Registered pxp with doc-base (Closes: Bug#137580).

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Mar 2002 13:18:49 +0100

pxp (1.1.3-3) unstable; urgency=low

  * Renamed package to libpxp-ocaml-dev

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 26 Feb 2002 10:26:21 +0100

pxp (1.1.3-2) unstable; urgency=low

  * Now build depends on ocaml >= 3.04-3, hopefully will compile also on
    ia64 and powerpc

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 11 Jan 2002 08:16:19 +0100

pxp (1.1.3-1) unstable; urgency=low

  * New upstream release

 -- Stefano Zacchiroli <zack@debian.org>  Thu, 20 Dec 2001 12:08:01 +0100

pxp (1.1.2-1) unstable; urgency=low

  * New upstream release
  * Built with ocaml 3.04

 -- Stefano Zacchiroli <zack@debian.org>  Sun, 16 Dec 2001 21:49:58 +0100

pxp (1.1-3) unstable; urgency=low

  * debian/rules now ignore errors due to missing native code compiler
  * removed .cvsignore files from binary package

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 17 Oct 2001 14:53:12 +0200

pxp (1.1-2) unstable; urgency=low

  * Removed dangling symlinks from examples/xmlforms/styles/ directory
    (closes: Bug#109265).
  * Rebuilt with ocaml 3.02

 -- Stefano Zacchiroli <zack@debian.org>  Mon, 20 Aug 2001 21:23:52 +0200

pxp (1.1-1) unstable; urgency=low

  * New upstream release.

 -- Stefano Zacchiroli <zack@debian.org>  Fri,  6 Jul 2001 23:58:34 +0200

pxp (1.0-4) unstable; urgency=low

  * Added pxp manual in HTML and PS format (closes: Bug#102906).

 -- Stefano Zacchiroli <zack@debian.org>  Sun,  1 Jul 2001 00:03:07 +0200

pxp (1.0-3) unstable; urgency=low

  * Added 'ocaml-netstring' to build-deps (closes: Bug#94558)

 -- Stefano Zacchiroli <zack@debian.org>  Fri, 20 Apr 2001 12:19:47 +0200

pxp (1.0-2) unstable; urgency=low

  * Native code compilation now is optional

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 10 Apr 2001 16:44:38 +0200

pxp (1.0-1) unstable; urgency=low

  * Initial Release.
  * Package released (closes: Bug#92713).
  * Package released (closes: Bug#92715).

 -- Stefano Zacchiroli <zack@debian.org>  Tue,  3 Apr 2001 15:09:28 +0200
